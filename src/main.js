var processFile = require('../src/lib/processing');
var parser = require('../src/lib/parser');

function parse(filePath, mime, cb){
    processFile.run(filePath, mime, function(err, processed){
    	if(err) return cb(err);
        parser.parse(processed ,function (resume) {
            cb(null, resume.parts);
        });
    });
}


module.exports  = {
    parse: parse
};
