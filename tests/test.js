var processFile = require('../src/lib/processing');
var parser = require('../src/lib/parser');

processFile.run(__dirname+'/stubs/my-resume.pdf', function(processed){
    parser.parse(processed ,function (r) {
        console.log(r);
    });
});